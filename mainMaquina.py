
M,N = 3,3
 
# Grid inicial vacia.
grid = [ [ 0, 0, 0 ],
         [ 0, 0, 0 ],
         [ 0, 0, 0 ]
]

# Funcion jugar para jugador 1 contra maquina
def jugar(valorJugador):
    
    if valorJugador==1:
        
        posicion=input("Jugador "+str(valorJugador)+" introduce la posicion del 1 al 9 :" )
        while int(posicion)<1 or  int(posicion)>9 or posicion in listaJugadas:
            posicion=input("Error, Jugador"+str(valorJugador)+" introduce la posicion del 1 al 9 :" )
    else:
        
        print ('Jugador 2 Maquina')
      
        posicion=buscarPosicion(grid)
        
    return posicion

def buscarPosicion(lista):
    #Evitar perder
    
    if lista[0][0] == '1' and lista[0][1] == '1' and lista[0][2] == 0:
        return'3'
    if lista[0][1] == '1' and lista[0][2] == '1' and lista[0][0] == 0:
        return'1'
    if lista[0][0] == '1' and lista[0][2] == '1' and lista[0][1] == 0:
        return'2'

    if lista[1][0] == '1' and lista[1][1] == '1' and lista[1][2] == 0:
        return'6'
    if lista[1][1] == '1' and lista[1][2] == '1' and lista[1][0] == 0:
        return'4'
    if lista[1][0] == '1' and lista[1][2] == '1' and lista[1][1] == 0:
        return'5'

    if lista[2][0] == '1' and lista[2][1] == '1' and lista[2][2] == 0:
        return'9'
    if lista[2][1] == '1' and lista[2][2] == '1' and lista[2][0] == 0:
        return'7'
    if lista[2][0] == '1' and lista[2][2] == '1' and lista[2][1] == 0:
        return'8'
    
    if lista[0][0] == '1' and lista[1][0] == '1' and lista[2][0] == 0:
        return'7'
    if lista[1][0] == '1' and lista[2][0] == '1' and lista[0][0] == 0:
        return'1'
    if lista[0][0] == '1' and lista[2][0] == '1' and lista[1][0] == 0:
        return'4'

    if lista[0][1] == '1' and lista[1][1] == '1' and lista[2][1] == 0:
        return'8'
    if lista[1][1] == '1' and lista[2][1] == '1' and lista[0][1] == 0:
        return'2'
    if lista[0][1] == '1' and lista[2][1] == '1' and lista[1][1] == 0:
        return'5'

    if lista[0][2] == '1' and lista[1][2] == '1' and lista[2][2] == 0:
        return'9'
    if lista[1][2] == '1' and lista[2][2] == '1' and lista[0][2] == 0:
        return'3'
    if lista[0][2] == '1' and lista[2][2] == '1' and lista[1][2] == 0:
        return'6'

    if lista[0][0] == '1' and lista[1][1] == '1' and lista[2][2] == 0:
        return'9'
    if lista[1][1] == '1' and lista[2][2] == '1' and lista[0][0] == 0:
        return'1'
    if lista[0][0] == '1' and lista[2][2] == '1' and lista[1][1] == 0:
        return'5'

    if lista[2][0] == '1' and lista[1][1] == '1' and lista[0][2] == 0:
        return'3'
    if lista[1][1] == '1' and lista[0][2] == '1' and lista[2][0] == 0:
        return'7'
    if lista[2][0] == '1' and lista[0][2] == '1' and lista[1][1] == 0:
        return'5'
    
    #Elegir mejores posiciones
    if lista[1][1] == 0:
        return '5'
    if lista[0][0] == 0:
        return '1'
    if lista[0][2] == 0:
        return '3'
    if lista[2][0] == 0:
        return '7'
    if lista[2][2] == 0:
        return '9'
    if lista[0][1] == 0:
        return '2'
    if lista[1][0] == 0:
        return '4'
    if lista[1][2] == 0:
        return '6'
    if lista[2][1] == 0:
        return '8'
            
#Chequeamos si alguno ha hecho 3 en raya  
def chequear(valorJugador, lista):
    gana=False
    unoDos=str(valorJugador)

    if (lista[0][0] == unoDos and lista[1][1] == unoDos and lista[2][2] == unoDos):
        gana=True
    elif (lista[0][2] == unoDos and lista[1][1] == unoDos and lista[2][0] == unoDos):
        gana=True
    elif (lista[0][0] == unoDos and lista[0][1] == unoDos and lista[0][2] == unoDos):
        gana=True
    elif (lista[1][0] == unoDos and lista[1][1] == unoDos and lista[1][2] == unoDos):
        gana=True
    elif (lista[2][0] == unoDos and lista[2][1] == unoDos and lista[2][2] == unoDos):
        gana=True
    elif (lista[0][0] == unoDos and lista[1][0] == unoDos and lista[2][0] == unoDos):
        gana=True
    elif (lista[0][1] == unoDos and lista[1][1] == unoDos and lista[2][1] == unoDos):
        gana=True
    elif (lista[0][2] == unoDos and lista[1][2] == unoDos and lista[2][2] == unoDos): 
        gana=True

    if gana :      
        print('Felicidades!!...jugador '+str(valorJugador)+' ha ganado!!!')

    return(gana)
 
#Guardo las jugadas en lista para chequear que no se repitan.
listaJugadas=[]
#Empieza a jugar el jugador 1 con X.
jugador=1
#Nueve jugadas, posiciones tablero.
for i in range (9):

    #Empieza el juego
    posi=jugar(jugador)
         
    listaJugadas.append(posi)


    # Paso a rellenar la grid de jugadas.
    if posi =='1':
        grid[0][0] = str(jugador)
    elif posi =='2':
        grid[0][1] = str(jugador)
    elif posi =='3':
        grid[0][2] = str(jugador)
    elif posi =='4':
        grid[1][0] = str(jugador)
    elif posi =='5':
        grid[1][1] = str(jugador)
    elif posi =='6':
        grid[1][2] = str(jugador)
    elif posi =='7':
        grid[2][0] = str(jugador)
    elif posi =='8':
        grid[2][1] = str(jugador)
    elif posi =='9':
        grid[2][2] = str(jugador)
    
    #Dibujar jugada
    for i in range(M):
        for j in range(N):
            if(grid[i][j] == 0):
                print(" . ",end = "")
            else:
                if grid[i][j]=='1':
                    print(" X ",end = "")
                else:
                    print(" O ",end = "")
        print()
    print()
    
    #Chequeo ganar
    fin=chequear(jugador, grid)
    if fin==True:
        break

    #Cambio de jugador
    if jugador==1:
        jugador=2
    else:
        jugador=1

print('Juego terminado!!!')



