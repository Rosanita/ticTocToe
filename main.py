from copy import deepcopy

M,N = 3,3
 
# Grid inicial vacia.
grid = [ [ 0, 0, 0 ],
         [ 0, 0, 0 ],
         [ 0, 0, 0 ]
]
#Grid donde guardare las jugadas.
nueva= deepcopy(grid)
#Guardo las jugadas en lista para chequear que no se repitan.
listaJugadas=[]
#Empieza a jugar el jugador 1 con X.
jugador=1
#Nueve jugadas, posiciones tablero.
for i in range (9):
    #Empieza el juego

    if jugador==1:
        posicion=input("Jugador1 introduce la posicion del 1 al 9 :" )

        while int(posicion)<1 or  int(posicion)>9 or posicion in listaJugadas:
            posicion=input("Error, Jugador1 introduce la posicion del 1 al 9 :" )
    else:
        posicion=input("Jugador2 introduce la posicion del 1 al 9 :" )

        while int(posicion)<1 or  int(posicion)>9 or posicion in listaJugadas:
            posicion=input("Error, Jugador2 introduce la posicion del 1 al 9 :" )
    
    listaJugadas.append(posicion)
    #Marco las jugadas con 1 para jugador 1 y 2 para jugador 2.
    if jugador==1:
        valor='1'
    else:
        valor='2'
    # Paso a rellenar la grid nueva de jugadas.
    if posicion =='1':
        nueva[0][0] = valor
    elif posicion =='2':
        nueva[0][1] = valor
    elif posicion =='3':
        nueva[0][2] = valor
    elif posicion =='4':
        nueva[1][0] = valor
    elif posicion =='5':
        nueva[1][1] = valor
    elif posicion =='6':
        nueva[1][2] = valor
    elif posicion =='7':
        nueva[2][0] = valor
    elif posicion =='8':
        nueva[2][1] = valor
    elif posicion =='9':
        nueva[2][2] = valor
    
    #Dibujar jugada
    for i in range(M):
        for j in range(N):
            
            if(nueva[i][j] == 0):
                print(" . ",end = "")
            else:
                if nueva[i][j]=='1':
                    print(" X ",end = "")
                else:
                    print(" O ",end = "")
        print()
    print()
    
    #Chequeo ganar
    gana=False
    if jugador==1:
        if (nueva[0][0] == '1' and nueva[1][1] == '1' and nueva[2][2] == '1'):
            gana=True
        elif (nueva[0][2] == '1' and nueva[1][1] == '1' and nueva[2][0] == '1'):
           gana=True
        elif (nueva[0][0] == '1' and nueva[0][1] == '1' and nueva[0][2] == '1'):
            gana=True
        elif (nueva[1][0] == '1' and nueva[1][1] == '1' and nueva[1][2] == '1'):
            gana=True
        elif (nueva[2][0] == '1' and nueva[2][1] == '1' and nueva[2][2] == '1'):
            gana=True
        elif (nueva[0][0] == '1' and nueva[1][0] == '1' and nueva[2][0] == '1'):
            gana=True
        elif (nueva[0][1] == '1' and nueva[1][1] == '1' and nueva[2][1] == '1'):
            gana=True
        elif (nueva[0][2] == '1' and nueva[1][2] == '1' and nueva[2][2] == '1'): 
            gana=True

        if gana :      
            print('Felicidades!!...jugador1 ha ganado!!!')
            break
    else:
        
        if (nueva[0][0] == '2' and nueva[1][1] == '2' and nueva[2][2] == '2'):
            gana=True
        elif (nueva[0][2] == '2' and nueva[1][1] == '2' and nueva[2][0] == '2'):
            gana=True
        elif (nueva[0][0] == '2' and nueva[0][1] == '2' and nueva[0][2] == '2'):
            gana=True
        elif (nueva[1][0] == '2' and nueva[1][1] == '2' and nueva[1][2] == '2'):
            gana=True
        elif (nueva[2][0] == '2' and nueva[2][1] == '2' and nueva[2][2] == '2'):
            gana=True
        elif (nueva[0][0] == '2' and nueva[1][0] == '2' and nueva[2][0] == '2'):
            gana=True
        elif (nueva[0][1] == '2' and nueva[1][1] == '2' and nueva[2][1] == '2'):
            gana=True
        elif (nueva[0][2] == '2' and nueva[1][2] == '2' and nueva[2][2] == '2'):
            gana=True

        if gana :
            print('Felicidades!!...jugador2 ha ganado!!!')
            break
    #Cambio de jugador
    if jugador==1:
        jugador=2
    else:
        jugador=1

print('Juego terminado!!!')



